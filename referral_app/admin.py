from django.contrib import admin
from .models import ReferralModel
from tbot_referral.telegram_config import BOT_NAME


class ReferralAdminModel(admin.ModelAdmin):
    list_display = ['referral_id', 'user_first_name', 'ref_link']
    list_display_links = ['referral_id']

    def ref_link(self, obj):
        return f'https://t.me/{BOT_NAME}?start={obj.referral_id}'

    ref_link.short_description = 'REF LINK'

    def user_first_name(self, obj):
        return obj.user.first_name

    user_first_name.short_description = 'USER'


admin.site.register(ReferralModel, ReferralAdminModel)
