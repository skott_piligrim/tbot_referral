from django.contrib.auth import get_user_model
from django.db import models
from model_utils.models import TimeStampedModel
from tbot_referral.telegram_config import BOT_NAME


User = get_user_model()


class ReferralModel(TimeStampedModel):
    class Meta:
        db_table = 'referral'
        verbose_name = 'Referral'
        verbose_name_plural = 'Referrals'

    user = models.OneToOneField(to=User, verbose_name='User', on_delete=models.CASCADE)
    referral_id = models.CharField(verbose_name='Referral id', unique=True, max_length=8)

    def __str__(self):
        return f'https://t.me/{BOT_NAME}?start={self.referral_id}'
