from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError
from referral_app.models import ReferralModel

User = get_user_model()


class Command(BaseCommand):
    def handle(self, *args, **options):
        users_first_name = [
            [{
                'username': 'admin',
                'first_name': 'Admin',
                'email': 'admin@gmail.com',
                'password': 'admin',
                'is_superuser': True,
                'is_staff': True,
            }, {'referral_id':'11111111'}],
            [{
                'username': 'ivan',
                'first_name': 'Ivan',
                'email': 'ivan@gmail.com',
                'password': 'ivan',
                'is_superuser': False,
            }, {'referral_id':'22222222'}],
            [{
                'username': 'petro',
                'first_name': 'Petro',
                'email': 'petro@gmail.com',
                'password': 'petro',
                'is_superuser': False,
            }, {'referral_id':'33333333'}],
            [{
                'username': 'vika',
                'first_name': 'Vika',
                'email': 'vika@gmail.com',
                'password': 'vika',
                'is_superuser': False,
            }, {'referral_id':'44444444'}],
            [{
                'username': 'lesya',
                'first_name': 'Lesya',
                'email': 'lesya@gmail.com',
                'password': 'lesya',
                'is_superuser': False,
            }, {'referral_id':'55555555'}],
            [{
                'username': 'roma',
                'first_name': 'Roma',
                'email': 'roma@gmail.com',
                'password': 'roma',
                'is_superuser': False,
            }, {'referral_id':'66666666'}]
        ]

        for item in users_first_name:
            try:
                instance = User.objects.create(**item[0])
                instance.set_password(item[0].get('password'))
                instance.save()
                ReferralModel.objects.create(user=instance, referral_id=item[1].get('referral_id'))
            except Exception as e:
                continue
