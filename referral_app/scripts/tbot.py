from typing import NoReturn
import telebot
from telebot import *
from tbot_referral.telegram_config import TOKEN
from referral_app.models import ReferralModel


def bot_work():
    bot = telebot.TeleBot(TOKEN)

    @bot.message_handler(commands=["start"])
    def start_command_handler(message):
        try:
            ref_id = message.text.split()[1]
        except IndexError:
            send_no_access_msg(message)
            return

        ref = ReferralModel.objects.select_related('user').filter(referral_id=ref_id).first()
        if not ref:
            send_forbidden_msg(message)
            return

        send_success_msg(message, ref)

    def send_success_msg(message, ref: ReferralModel) -> NoReturn:
        bot.send_message(message.chat.id,
                         f"Приветствую {ref.user.first_name}! Твой уникальный код - {ref.user_id}")

    def send_forbidden_msg(message) -> NoReturn:
        bot.send_message(message.chat.id, "Недействительная реферальная ссылка.")

    def send_no_access_msg(message) -> NoReturn:
        bot.send_message(message.chat.id,
                         "Вы не можете начать использовать бот без реферальной ссылки. Перейдите в бот с помощью реферальной ссылки")

    bot.polling(none_stop=True, interval=0)


def run(*args):
    bot_work()


