To start the project, you first need to run the command:
docker-compose build
Then
docker-compose up

Admin access:
Login: admin
Password: admin

After the launch, there are already several users in the database with their referral links.
Example: https://i.imgur.com/uqOEHUu.png

Bot link: @referal_rtest_bot
